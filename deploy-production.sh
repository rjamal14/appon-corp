cd /home/deploy/container/appon-corp

git checkout production
git pull origin production

docker pull apponid/appon-corp:production
docker stack deploy --with-registry-auth -c docker-compose-production.yml appon-corp --prune
sleep 10
docker system prune -a -f

echo 'Deploy appon-corp finished.'
