FROM node:12 as dependencies
WORKDIR /appon-corp
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

FROM node:12 as builder
ARG REACT_APP_BASE_URI
ARG REACT_APP_AUTH_URI
ARG REACT_APP_MASTER_URI
ARG REACT_APP_ONLINK_URI
ARG REACT_APP_TRANSACTION_URI
ARG REACT_APP_CLIENT_ID
ARG REACT_APP_CLIENT_SECRET
WORKDIR /appon-corp
RUN touch /appon-corp/.env
RUN echo "REACT_APP_BASE_URI=${REACT_APP_BASE_URI}" >> /appon-corp/.env
RUN echo "REACT_APP_AUTH_URI=${REACT_APP_AUTH_URI}" >> /appon-corp/.env
RUN echo "REACT_APP_MASTER_URI=${REACT_APP_MASTER_URI}" >> /appon-corp/.env
RUN echo "REACT_APP_ONLINK_URI=${REACT_APP_ONLINK_URI}" >> /appon-corp/.env
RUN echo "REACT_APP_TRANSACTION_URI=${REACT_APP_TRANSACTION_URI}" >> /appon-corp/.env
RUN echo "REACT_APP_CLIENT_ID=${REACT_APP_CLIENT_ID}" >> /appon-corp/.env
RUN echo "REACT_APP_CLIENT_SECRET=${REACT_APP_CLIENT_SECRET}" >> /appon-corp/.env
COPY . .
COPY --from=dependencies /appon-corp/node_modules ./node_modules
RUN yarn build

FROM node:12 as runner
WORKDIR /appon-corp
COPY --from=builder /appon-corp/.env ./.env
COPY --from=builder /appon-corp/public ./public
COPY --from=builder /appon-corp/src ./src
COPY --from=builder /appon-corp/node_modules ./node_modules
COPY --from=builder /appon-corp/package.json ./package.json
EXPOSE 3000
CMD ["yarn", "start"]