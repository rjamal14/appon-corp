cd appon-corp
git checkout development
git pull origin development
docker-compose down
docker-compose rm -f
docker system prune -a -f
docker pull apponid/appon-corp:development
docker-compose pull
docker-compose up --build --force-recreate -d
echo 'Deploy appon-corp finished.'
