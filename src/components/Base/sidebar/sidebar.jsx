import React, { useEffect } from "react";
import { useLocation, withRouter, Link } from "react-router-dom";
import "./sidebar.css";
import "./_vertical.scss";
import {
  FaLandmark,
  FaFileContract,
  FaStore, 
  FaChartLine, 
  FaClipboardList,
  FaDatabase} from 'react-icons/fa'
import logo from "../../../assets/appon/garuda.png";
import logo_foot from "../../../assets/appon/logowebhitam@2x.png";
import MetisMenu from "metismenujs";
import loading from "../../../assets/loading.gif";
import { useSelector } from "react-redux";
import env from '../../../config/env';
import Swal from "sweetalert2";

const Sidebar = ({ sidebarOpen, closeSidebar }) => {

  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");
  const instansi = useSelector(state => state.instansi);

  useEffect(() => {
    initMenu();
  }, [pathname]);

  function noOnlink() {
    Swal.fire({
      title: 'Informasi',
      text: 'Instansi anda belum terhubung pada katalog, mohon menghubungi AppOn terlebih dahulu',
      icon: 'info',
      confirmButtonColor: '#16935B',
      confirmButtonText: 'Okay'
    })
  }

  function initMenu(){
    // new MetisMenu("#side-menu");

    var matchingMenuItem = null;
    var ul = document.getElementById("side-menu");
    var items = ul.getElementsByTagName("a");
    for (var i = 0; i < items.length; ++i) {
      if (pathname === items[i].pathname) {
        matchingMenuItem = items[i];
        break;
      }
    }
    if (matchingMenuItem) {
      activateParentDropdown(matchingMenuItem);
    }
  }

  function activateParentDropdown(item){
    item.classList.add("mm-active");
    item.classList.add("mm-act-bdr");
    const parent = item.parentElement;

    if (parent) {
      parent.classList.add("mm-active"); // li
      const parent2 = parent.parentElement;

      if (parent2) {
        // parent2.classList.add("mm-show");

        const parent3 = parent2.parentElement;

        if (parent3) {
          parent3.classList.add("mm-active"); // li
          parent3.childNodes[0].classList.add("mm-active"); //a

          const parent4 = parent3.parentElement;
          if (parent4) {
            parent4.classList.add("mm-active");
            parent4.childNodes[0].classList.add("mm-active"); //a
          }
        }
      }
      return false;
    }
    return false;
  };

  return (
    <div className={sidebarOpen ? "sidebar__responsive shadow" : "shadow"} id="sidebar-a">
      <div className="sidebar__title">
          <div className="sidebar__img">
            {/* {instansi.instansi.cover_url ? */}
              <img src={instansi.instansi.cover_url} alt="logo" />
              {/* :
              <img className="img-profil-load" src={loading} alt="logo" />
            } */}
            <h1>{instansi.instansi.name}</h1>
          </div>
        <i
          className="fa fa-times"
          id="sidebarIcon"
          onClick={() => closeSidebar()}
        ></i>
      </div>
      <center className="mb-30">
        {
          instansi.instansi?.onlink?.link ?
          <a className="title-statcard" target="_blank" href={env.onlinkUrl+instansi.instansi.onlink.link}>
            <FaStore className="me-1" /> Katalog UMKM
          </a>
          :
          <span className="title-statcard" onClick={()=>{noOnlink()}}>
            <FaStore className="me-1" /> Katalog UMKM
          </span>
        }
      </center>
      <div className="sidebar__menu" id="sidebar-menu">
      <ul className="metismenu list-unstyled" id="side-menu">
        {/* <li className="menu-title">title</li> */}        
        <li>
          <Link to="/">
            <FaChartLine />
            {/* <span className="badge badge-pill badge-danger float-right">2</span> */}
            <span>Info Grafis</span>
          </Link>
        </li>
        <li>
          <Link to="#">
            <FaLandmark />
            {/* <span className="badge badge-pill badge-danger float-right">2</span> */}
            <span>Data Pengusaha</span>
          </Link>
          <ul className="sub-menu">
            <li>
              <Link to="stakeholder-import">- Import Data</Link>
            </li>
            <li>
              <Link to="stakeholder-rekap">- Rekap Data</Link>
            </li>
          </ul>
        </li>
        <li>
          <Link to="#`">
            <FaStore />
            {/* <span className="badge badge-pill badge-danger float-right">2</span> */}
            <span>Data Wirausaha</span>
          </Link>
          <ul className="sub-menu">            
            <li>
              <Link to="#">
                {/* <FaStore /> */}
                {/* <span className="badge badge-pill badge-danger float-right">2</span> */}
                <span>- Rekap Data</span>
              </Link>
              <ul className="sub-menu">
                {instansi.instansi.zone_id != null && <li>
                  <Link to="umkm">UMKM Non Binaan</Link>
                </li>}
                <li>
                  <Link to="umkm-binaan">UMKM Binaan</Link>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/program">
                {/* <FaClipboardList /> */}
                <span>- Program</span>
              </Link>
              <ul className="sub-menu">
                <li>
                  <Link to="program-data"> Data Program</Link>
                </li>
                <li>
                  <Link to="program-kolaborasi"> Program Kolaborasi</Link>
                </li>
                <li>
                  <Link to="program-tambah"> Tambah Program</Link>
                </li>
              </ul>
            </li>

            <li>
              <Link to="/monitoring">
                {/* <FaChartLine /> */}
                <span>- Monitoring</span>
              </Link>
              <ul className="sub-menu">
                <li>
                  <Link to="program-on-progress"> Program On Progress</Link>
                </li>
                <li>
                  <Link to="program-ditangguhkan"> Program Ditangguhkan</Link>
                </li>
                <li>
                  <Link to="program-selesai"> Program Selesai</Link>
                </li>
              </ul>
            </li>
          </ul>
        </li>

        
        {/* <li>
          <Link to="/tugas" className="waves-effect">
            <FaClipboardList />
            <span>Daftar Tugas</span>
          </Link>
        </li>

        <li>
          <Link to="/laporan" className="waves-effect">
            <FaReceipt />
            <span>Laporan</span>
          </Link>
        </li>

          {localStorage.getItem('cooperation_status') === "profit_sharing" && <li>
          <Link to="/#">
            <FaWallet />
            <span>Dompet</span>
          </Link>
          <ul className="sub-menu">
            <li>
              <Link to="komisi"> Komisi</Link>
            </li>
            <li>
              <Link to="pencairan"> Pencairan</Link>
            </li>
          </ul>
        </li>}
        
        <li>
          <Link to="/#">
            <FaDatabase />
            <span>Pengaturan</span>
          </Link>
          <ul className="sub-menu">
            <li>
              <Link to="paket-tugas"> Paket Tugas</Link>
            </li>
          </ul>
        </li> */}
      </ul>
      </div>
      <div className="sidebar__footer">
        <span>Powered By :</span>
        <img className="img-fluid mt-1" src={logo_foot} alt="" />
      </div>
    </div>
  );
};

export default withRouter(Sidebar);
