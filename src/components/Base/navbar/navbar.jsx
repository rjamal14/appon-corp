import React from "react";
import { Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";
import { signout } from "../../../actions";
import { FaSignOutAlt,FaUserCog } from 'react-icons/fa'
import "./Navbar.css";
import avatar from "../../../assets/avatar.svg";

const Navbar = ({ sidebarOpen, openSidebar }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const instansi = useSelector(state => state.instansi);
  const user = localStorage.getItem('user');
  
  const logout = () => {
    dispatch(signout());
  };
  return (
    <nav className="navbar">
      <div className="nav_icon" onClick={() => openSidebar()}>
        <i className="fa fa-bars"></i>
      </div>
      <div className="navbar__left">
      </div>
      <div className="navbar__right">        
        <Dropdown>
          <Dropdown.Toggle variant="success" id="dropdown-basic" className="navbar-dropdown">
            <img width="30" src={instansi.instansi.cover_url} alt="avatar" className="rounded-circle"/>
            <span> Hallo, {user && JSON.parse(user)?.name}</span>
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item as={Link} to="/profil">
              <FaUserCog />Profil
            </Dropdown.Item>
            <Dropdown.Item href="#" onClick={logout}>
              <FaSignOutAlt />Keluar
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </nav>
  );
};

export default Navbar;
