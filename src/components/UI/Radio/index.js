import React from 'react';
import { Form } from 'react-bootstrap';
import ToggleButton from "react-bootstrap/ToggleButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";

/**
* @author
* @function Radio
**/

const Radio = (props) => {
  return (
    <>
      <ButtonGroup toggle>
        {props.radios.map((radio, index) => (
          <ToggleButton
            key={index}
            type="radio"
            name={radio.name}
            value={radio.value}
            checked={radio.value}
            onChange={props.onChange}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
    </>
  );

 }

export default Radio