import React, { useEffect } from 'react';
import Layout from '../../../components/Base/main/main';
import StatCard from "./StatCard";
import MarketPlace from "./MarketPlace";
import Medsos from "./Medsos";
import Bidangindustri from "./Bidangindustri";
import Kategori from "./Kategori";
import Loker from "./Loker";
import Badanhukum from "./Badanhukum";
import Jenisusaha from "./Jenisusaha";
import Jenisusaha2 from "./Jenisusaha2";
import Pegawai from "./Pegawai";
import "./style.css"
import { useDispatch, useSelector } from 'react-redux';
import { 
  getDataRecap,
  getDataRecapBusinessmans,
  getDataRecapTrainersCount,
  getDataRecapUMKM
} from '../../../actions/dashboard.actions';

function Rekap() {
  // const { setHeaderComponent } = useContext(LayoutContext);

  // useEffect(() => {
  //   setHeaderComponent(<b>Rekap Data</b>);
  //   return () => {
  //     setHeaderComponent(null);
  //   };
  // }, []);

  const user = JSON.parse(localStorage.getItem("user"))
  const dispatch = useDispatch()

  useEffect(() => {
    const instanceId = user.user_instansi[0].instansi_id
    dispatch(getDataRecap(instanceId))
    dispatch(getDataRecapBusinessmans(instanceId))
    dispatch(getDataRecapTrainersCount(instanceId))
    dispatch(getDataRecapUMKM(instanceId))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  
  return (
    <Layout>
      <div className="row">
        <StatCard color="redap" />
        <Kategori stat="Kategori UMKM" color="white" />
        <Loker stat="Rata-rata Lapangan Pekerjaan" color="white" />
        <Bidangindustri stat="Bidang Industri" label="31 Jenis" color="white" />
        <Badanhukum stat="Badan Hukum" label="3 Jenis" color="white" />
        <Jenisusaha stat="Karakteristik Usaha" label="3 Jenis" color="white" />
        <Jenisusaha2 stat="Unit Usaha" label="2 Jenis" color="white" />
        <MarketPlace stat="Marketplace Terpopuler" color="white" />
        <Medsos stat="Media Sosial Terpopuler" color="white" />
      </div>
    </Layout>
  );
}

export default Rekap;
