import React from 'react';
import ReactEcharts from "echarts-for-react"
import { useSelector } from 'react-redux';

function Pegawai(props) {
  let bgColor = "bg-" + props.color;
  const data = useSelector(state => state.dashboard.data_recap_trainers_count)
  let dataY = [];
  let dataX = [];
  if (data?.jumlah_pegawai) {
    Object.keys(data.jumlah_pegawai).map(function(key, index) {
      switch (key) {
        case 'less_than_5':
          dataY[0] = '< 5 Orang';
          dataX[3] = { value: data.jumlah_pegawai[key], itemStyle: {color: '#F2334E'}};
        break;
        case '5_to_19':
          dataY[1] = '5-19 Orang';
          dataX[2] = { value: data.jumlah_pegawai[key], itemStyle: {color: '#FFA127'}};
        break;
        case '20_to_99':
          dataY[2] = '20-99 Orang';
          dataX[1] = { value: data.jumlah_pegawai[key], itemStyle: {color: '#FFA127'}};
        break;
        case 'more_than_100':
          dataY[3] = '> 100 Orang';
          dataX[0] = { value: data.jumlah_pegawai[key], itemStyle: {color: '#16935B'}};
        break;
      }
    });
  }
  function getOption(){
    return {
        tooltip: {
          trigger: "item",
          formatter: "{b} <br/>{c} {a}",
          axisPointer: {    
              type: 'shadow'
          }
        },
        grid: {
            top: 10,
            bottom: 30,
            left: 70,
            right: 40
        },
        xAxis: {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: dataY,
            axisLabel: {
                show: true,
                textStyle: {
                    fontSize: 10
                },
                rich: {
                    flag: {
                        fontSize: 5,
                        padding: 2
                    }
                }
            },
            axisTick: {
                alignWithLabel: true
            }
        },
        series: [{
            name: 'UMKM',
            data: dataX,
            type: 'bar',
            inverse: false,
            label: {
                show: true,
                precision: 1,
                position: 'right',
                valueAnimation: true,
                fontFamily: 'poppins'
            }
        }]
    }
  }
  return (
    <div className="col-md-6 col-md-4">
      <div className={"card card-hg mb-3 border-0 rounded-3 shadow " + bgColor}>
        <div className="card-body">
          <div className="card-header1">
            <h5>{props.stat}</h5>
            <h6>{props.label}</h6>
          </div>
          <div className="card-content">
            <ReactEcharts style={{ height: "170px" }} option={getOption()} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Pegawai;