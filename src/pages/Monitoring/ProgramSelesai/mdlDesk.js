import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Input from "../../../components/UI/Input";
import Modal from "../../../components/UI/Modal";
import { FaRegCheckSquare, FaRegSquare } from 'react-icons/fa';
import Swal from "sweetalert2";

const MdlDesk = ({ isShow, handleClose, handleSave, state }) => {
  const [ket, setKet] = useState('')

  useEffect(() => {
   if(state=='check'){
     setKet('Selamat, anda terverifikasi, pihak instansi penyedia program ini akan segera menghubungi anda.')
   } else {
      setKet('Anda belum lolos seleksi program ini, jangan khawatir anda masih bisa mengikuti program lainnya, lengkapi agenda segera agar untuk kemudahan mengikuti program-program berikutnya.')
   }
  }, [state])

  const onSave = () => {
    if (!ket=='') {
      Swal.fire({
          title: '',
          text: "Apakah anda sudah yakin?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Simpan',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Edit Ulang',
      
        }).then((result) => {
          if (result.isConfirmed) {
              handleSave(ket)
          }
        })
    } else {
      Swal.fire({
        text: 'Anda belum mengisi catatan',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    }
  }

  return(
    <>
      <Modal modalTitle={state=="check"?"Catatan Seleksi Terverifikasi Program":"Catatan Seleksi Tidak Terverifikasi Program"} show={isShow} handleClose={() => handleClose()}>
        <div className="row">
          <div className="col-md-12">
              <Input
                type="textarea"
                rows={5}
                label="Catatan"
                placeholder="silahkan isi Catatan anda disini"
                name="ket"
                value={ket}
                onChange={(event)=>setKet(event.target.value)}
              />
          </div>
          <div className="col">
            <div className="float-right">
              {!state==='view'?
                <button className="btn btn-sm button-secondary mr-2" onClick={() => handleClose()}>Tutup</button>
                :
                <>
                <button className="btn btn-sm button-primary mr-2" onClick={() => handleClose()}>Batal</button>
                <button className="btn btn-sm button-green" onClick={() => onSave()}>Simpan</button>
                </>
              }      
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default MdlDesk