import React, { useEffect } from 'react';
import Layout from '../../../components/Base/main/main';
import SuccessRate from "./SuccessRate";
import AbsorptionBudget from "./AbsorptionBudget";
import "./style.css"
import { useDispatch, useSelector } from 'react-redux';
import { getDataMonitoring} from '../../../actions/dashboard.actions';

function Rekap() {
  // const { setHeaderComponent } = useContext(LayoutContext);

  // useEffect(() => {
  //   setHeaderComponent(<b>Rekap Data</b>);
  //   return () => {
  //     setHeaderComponent(null);
  //   };
  // }, []);

  const user = JSON.parse(localStorage.getItem("user"))
  const dispatch = useDispatch()

  useEffect(() => {
    const instanceId = user.user_instansi[0].instansi_id
    dispatch(getDataMonitoring(instanceId))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  
  return (
    <Layout>
      <div className="row">
        <SuccessRate stat="Tingkat Keberhasilan" label="31 Jenis" color="white" />
        <AbsorptionBudget stat="Serapan Anggaran" label="31 Jenis" color="white" />
      </div>
    </Layout>
  );
}

export default Rekap;
