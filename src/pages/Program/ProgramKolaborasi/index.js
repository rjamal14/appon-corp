import React, { useEffect, useState } from "react";
import Layout from '../../../components/Base/main/main';
import { FaCogs, FaSearch, FaEye, FaTrashAlt } from 'react-icons/fa'
import { Modal, Form, Dropdown } from "react-bootstrap";
import Input from "../../../components/UI/Input";
import { Tabs, Tab } from 'react-bootstrap';
import './style.scss';
import FormDet from "../ProgramData/formDetail";
import DataTable from '../../../components/UI/Table/Table';
import { useDispatch, useSelector } from "react-redux";
import { getProgram } from "../../../actions/program.actions";
import moment from 'moment';

function ProgramData() {
  const [search, setSearch] = useState('')
  const [jenis, setJenis] = useState('')
  const [status, setStatus] = useState('')
  const [activeTab, setActiveTab] = useState('all');
  const [selectedProgram, setSelectedProgram] = useState({})
  const [showFormDetail, setShowFormDetail] = useState(false);
  const program = useSelector(state => state.program)
  const user = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const j_program = [{
    value:'',
    name:'Pilih Jenis Program',
    disabled:true,
    selected:true
  },{
    value:'all',
    name:'Semua'
  },{
    value:'1',
    name:'Pendanaan Pinjaman'
  },
  {
    value:'2',
    name:'Pendanaan Hibah'
  },
  {
    value:'3',
    name:'Pendanaan Kompetisi'
  },
  {
    value:'4',
    name:'Pameran/event'
  },
  {
    value:'5',
    name:'Pelatihan/Pengembangan'
  },
  {
    value:'6',
    name:'Pengadaan'
  }]
  
  const s_program = [{
    value:'',
    name:'Pilih Status Program',
    disabled:true
  },{
    value:'all',
    name:'Semua',
    selected:true
  },
  {
    value:'draft',
    name:'Draft'
  },
  {
    value:'scheduled',
    name:'Dijadwalkan'
  },
  {
    value:'progress',
    name:'Progress'
  },
  {
    value:'complete',
    name:'Selesai'
  },
  {
    value:'suspend',
    name:'Ditangguhkan'
  }]
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const list = program?.program?.map((i, index) => {
    return {
      col1: (((page - 1) * perPage) + (index + 1)).toString(),
      col2: i.program_name,
      col3: i?.program_owners[0]?.instansi?.name,
      col4: moment(i.program_start).format('DD-MM-YYYY HH:mm')+' WIB',
      col5: moment(i.program_end).format('DD-MM-YYYY HH:mm')+' WIB',
      col6: rendStatus(i.status),
      col7: i.participant_quota,
      col8: i.program_quota,
      col9: i.id,
      col10: i.program_category?.name
    };
  });

  function rendStatus(val){
    switch (val) {
      case 'suspend':
        return 'Ditangguhkan'
      break;    
      case 'complete':
        return 'Selesai'
      break;  
      default:
        return val.charAt(0).toUpperCase() + val.slice(1);
      break;
    }
  }

  function setType(val){
    switch (val) {
      case 'pinjaman':
        return 'Pendanaan Pinjaman'
      break;    
      case 'hibah':
        return 'Pendanaan Hibah'
      break;    
      case 'kompetisi':
        return 'Pendanaan Kompetisi'
      break;    
      case 'pameran':
        return 'Pameran/Event'
      break;    
      case 'pelatihan':
        return 'Pelatihan/Pengembangan'
      break;    
      case 'pengadaan':
        return 'Pengadaan'
      break;    
    }
  }
  
  const data = React.useMemo(() => list, [program?.program]);
  const columns = React.useMemo(
    () => [
      {
        Header: 'No',
        accessor: 'col1',
        width: '5%',
        textAlign: 'center'
      },
      {
        Header: 'Nama Program',
        accessor: 'col2'
      },
      {
        Header: 'Pemilik Program',
        accessor: 'col3',
      },
      {
        Header: 'Jenis Program',
        accessor: 'col10',
      },
      {
        Header: 'Waktu Mulai',
        accessor: 'col4',
        width: '10%',
        textAlign: 'center'
      },
      {
        Header: 'Waktu Selesai',
        accessor: 'col5',
        width: '10%',
        textAlign: 'center'
      },
      {
        Header: 'Status',
        accessor: 'col6',
        width: '8%',
        textAlign: 'center',
        className:'capitalize'
      },
      {
        Header: 'Kuota',
        accessor: 'col7',
        width: '8%',
        textAlign: 'center'
      },
      {
        Header: 'Jumlah Peserta',
        accessor: 'col8',
        width: '8%',
        textAlign: 'center'
      },
      {
        Header: 'Detail',
        accessor: 'col9',
        Cell: (item) => (                 
          <div className="btn-setting">
            <Dropdown className="dpw-setting">
              <Dropdown.Toggle variant="setting" id="dropdown-basic">
                <FaCogs className="me-1" />
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item href="#" onClick={() => {
                  handleFormDetail(item.cell.value,item.customState.listProgram,'view')
                }}>
                  <FaEye />
                  Detail
                </Dropdown.Item>
                <Dropdown.Item href="#" onClick={() =>{
                }}>
                  <FaTrashAlt />
                  Hapus
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        ),
        width: '5%',
        textAlign: 'center'
      }
    ],
    [program]
  );

  useEffect(() => {
    let parm = {
      status:status,
      is_owner:0,
      jenis:jenis,
      search:search
    }
    dispatch(getProgram(page, perPage, parm))
  }, [page, perPage, status, jenis, search])

  const handleFormDetail = (id,data,state) => { 
    const data_program = data.find(item => item.id === id);
    setSelectedProgram(data_program);
    switch (state) {
      case 'view':
        setShowFormDetail(true);
      break;
    }
  }

  return (
    <>
    <Layout>
    <div className="row table-title">
      <span className="title">Program Kolaborasi</span>
      <span className="desk">Program {'>'} Program Kolaborasi</span>
    </div>
    <div className="row table-view">
      <div className="col">
        <div className="col-wrapper">
          <div className="text-start">
            <div className="search-wrapper src-wrap">
              <input id="search-input" placeholder="Cari program, pemilik program dll.." type="text" onChange={(e) => setSearch(e.target.value)}/>
              <button className="btn btn-sm btn-outline-secondary" onClick={() => setSearch(search)}>
                <FaSearch className="me-1" />
              </button>                  
            </div>
            <Input
              type="select"
              classGroupName="filter_jenis"
              label=""
              name="jenis"
              options={j_program}
              onChange={(e) => {
                setJenis(e.target.value);
              }}
            />
            <Input
              type="select"
              label=""
              name="status"
              options={s_program}
              onChange={(e) => {
                setStatus(e.target.value);
              }}
            />
          </div>
        </div>
        <DataTable
          data={data}
          columns={columns}
          loading={program.loading}
          customState={{listProgram:program?.program??[]}}
          setPage={setPage}
          setPerPage={setPerPage}
          currentpage={page}
          perPage={perPage}
          totalPage={program?.meta?.last_page}
        />
      </div>
    </div>      
    </Layout>
    <FormDet 
      isShow={showFormDetail}
      data={selectedProgram}
      handleClose={() => setShowFormDetail(false)}
    />
    </>
  );
}

export default ProgramData;