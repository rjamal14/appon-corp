import React, { useEffect } from 'react';
import Layout from '../../../components/Base/main/main';
import StatCard from "./StatCard";
import JenisIndustri from "./JenisIndustri";
import StatusProgram from "./StatusProgram";
import Peserta from "./Peserta";
import Bidangindustri from "./Bidangindustri";
import "./style.css"
import { useDispatch, useSelector } from 'react-redux';
import { 
  getDataProgram
} from '../../../actions/dashboard.actions';

function Rekap() {
  const user = JSON.parse(localStorage.getItem("user"))
  const dispatch = useDispatch()

  useEffect(() => {
    const instanceId = user.user_instansi[0].instansi_id
    dispatch(getDataProgram(instanceId))
  }, [])
  
  return (
    <Layout>
      <div className="row">
        <StatCard color="redap" />
        <Bidangindustri stat="Perbandingan Program"  color="white" />
        <JenisIndustri stat="Peserta Berdasarkan Jenis Industri" label="31 Jenis" color="white" />
        <StatusProgram stat="Status Program" label="3 Jenis" color="white" />
        <Peserta stat="Peserta Binaan" label="3 Jenis" color="white" />
        {/*<Medsos stat="Media Sosial Terpopuler" color="white" />
        <Badanhukum stat="Badan Hukum" label="3 Jenis" color="white" />
        <Jenisusaha stat="Karakteristik Usaha" label="3 Jenis" color="white" />
        <Jenisusaha2 stat="Unit Usaha" label="2 Jenis" color="white" /> */}
      </div>
    </Layout>
  );
}

export default Rekap;
