export const setNumberFormat = (val) => {
  return val>0?val.toLocaleString():0;
};
export const setStatus = (val) => {
  switch (val) {
    case 'scheduled':
      return 'Dijadwalkan'
    break;    
    case 'suspend':
      return 'Ditangguhkan'
    break;    
    case 'complete':
      return 'Selesai'
    break;  
    default:
      return val.charAt(0).toUpperCase() + val.slice(1);
    break;
  }
}

export const setType = (val) => {
  switch (val) {
    case 'pinjaman':
      return 'Pendanaan Pinjaman'
    break;    
    case 'hibah':
      return 'Pendanaan Hibah'
    break;    
    case 'kompetisi':
      return 'Pendanaan Kompetisi'
    break;    
    case 'pameran':
      return 'Pameran/Event'
    break;    
    case 'pelatihan':
      return 'Pelatihan/Pengembangan'
    break;    
    case 'pengadaan':
      return 'Pengadaan'
    break;    
    default:
      return '';
    break;
  }
}

export const isBase64 = (str) => {
  var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
  return base64Matcher.test(str);
}