import React, { useEffect, useState } from "react";
import { FaCogs,  } from 'react-icons/fa'
import { Dropdown } from "react-bootstrap";
import Layout from '../../components/Base/main/main';
import logo from "../../assets/appon/garuda.png";
import loading from "../../assets/loading.gif";
import { useDispatch, useSelector } from "react-redux";
import { getProfil } from "../../actions/instansi.actions";
import './style.scss';

import UpdateProfil from "./updateProfil";

function Profil() {
    const [updatelogo, setUpdatelogo] = useState(false);
    const instansi = useSelector(state => state.instansi);
    const user = JSON.parse(localStorage.getItem("user"));
    const [showUpdateForm, setShowUpdateForm] = useState(false);

    const dispatch = useDispatch()
    
    useEffect(() => {
        const instanceId = user.user_instansi[0].instansi_id
        dispatch(getProfil(instanceId))
    }, [])

    const handleEditLogo = () => {
        setUpdatelogo(true)
        setShowUpdateForm(true)
    }

    return (
        <Layout>      
        <div className="col-md-6">
            <div className="col-wrapper-profile">
                <div className="profil row">           
                    <span className="span-img-profil">
                        {instansi.loading && <img className="img-profil" src={loading} alt="logo" />}
                        {!instansi.loading && <img className="img-profil" src={instansi.instansi.cover_url!==null?instansi.instansi.cover_url:logo} alt="logo" />}
                    </span>      
                    <div className="det-profil">    
                        <span className="span-status">Nama Instansi</span>
                        <span className="span-nama">{!instansi.loading && instansi.instansi.name}</span>
                    </div>  
                    <div className="det-setting">
                        <Dropdown className="dpw-setting">
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                <FaCogs className="me-1" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item href="#" onClick={() => handleEditLogo()}>
                                    Edit Logo
                                </Dropdown.Item>
                                <Dropdown.Item href="#" onClick={() => {
                                    setShowUpdateForm(true)
                                    setUpdatelogo(false)
                                }}>
                                    Edit Nama
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>   
                    </div>  
                </div>
            </div>        
        </div>
        <UpdateProfil data={instansi.instansi} isShow={showUpdateForm} isLogo={updatelogo} handleClose={() => setShowUpdateForm(false)}/>
        </Layout>
    );
}

export default Profil;
