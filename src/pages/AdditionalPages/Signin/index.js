import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';
import { Card, CardBody, Alert } from "reactstrap";
import Input from '../../../components/UI/Input';
import { login } from '../../../actions/auth.actions';
import loading from "../../../assets/loading.gif";
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import './style.scss';
import Swal from "sweetalert2";


import bg from "../../../assets/appon/login-bg.png";
import logoDark from "../../../assets/appon/logo.png";

/**
* @author
* @function Signin
**/

const Signin = (props) => {
    const [username, setUsername] = useState('');
    const [loadingauth, setLoading] = useState(false);
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const auth = useSelector(state => state.auth);

    const dispatch = useDispatch();

    const userLogin = (e) => {
        e.preventDefault();

        const user = {
            username, password
        }
        setLoading(true)
        dispatch(login(user))
      .then((res) => {
      })
      .catch((err) => {
        setLoading(false)
        Swal.fire({
          text: 'Username atau Password Salah ',
          icon: 'error',
          confirmButtonColor: '#16935B',
          confirmButtonText: 'Ok'
        })
      })
    }

    if(auth.authenticate){
        return <Redirect to={`/`} />
    }

    return (
      <div className="login-wrapper">
      <div
          className="accountbg"
          style={{
            backgroundSize: "cover",
            backgroundPosition: "center",
            backgroundImage: `url(${bg})`,
            width: '75vw'
          }}
        ></div>
        <div className="wrapper-page account-page-full">
          <Card className="shadow-none">
            <div className="card-block">
              <div className="account-box">
                <div className="card-box shadow-none p-4">
                  <div className="p-2">
                    <div className="text-center mt-4">
                      <Link to="/">
                        <img className="auth-logo" src={logoDark} height="22" alt="logo" />
                      </Link>
                    </div>

                    <span className="font-size-12 mt-2 text-center">
                      Selamat Datang di Appon Corp
                    </span>
                    <p className="text-muted text-center">
                      {/* Sign in to continue to Appon. */}
                    </p>

                    <Form className="form-login" onSubmit={userLogin}>
                        <Input 
                            placeholder="username"
                            value={username}
                            type="text"
                            onChange={(e) => setUsername(e.target.value)}
                        />
                        <Input 
                            placeholder="password"
                            value={password}
                            type="password"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {loadingauth?                        
                        <img className="img-login-load" src={loading} alt="logo" />
                        :
                        <Button className="btn-login btn-secondary" type="submit">
                            Masuk
                        </Button>
                        }
                        <Row className="form-group mt-2 mb-0">
                          <div className="col-12 mt-4">
                            <Link to="/forget-password">
                              <i className="mdi mdi-lock"></i> Lupa
                              password?
                            </Link>
                          </div>
                        </Row>
                      </Form>   
                    <div className="mt-5 pt-4 text-center">
                      {/* <p>
                        Don't have an account ?{" "}
                        <Link
                          to="pages-register-2"
                          className="font-weight-medium text-primary"
                        >
                          {" "}
                          Signup now{" "}
                        </Link>{" "}
                      </p> */}
                      <p>
                        © {new Date().getFullYear()} AppOn.id
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    )    
}

export default Signin