import React, { useEffect, useState } from "react";
import Layout from '../../../components/Base/main/main';
import { FaCogs, FaPencilAlt, FaEye, FaTrashAlt, FaTrashRestoreAlt } from 'react-icons/fa'
import { Dropdown } from "react-bootstrap";
import { Tabs, Tab } from 'react-bootstrap';
import './style.scss';
import ErrorRes from "../../../components/UI/ErrorRes";
import DataTable from '../../../components/UI/Table/Table';
import { useDispatch, useSelector } from "react-redux";
import { getProgram, deleteProgram, restoreProgram } from "../../../actions/program.actions";
import { cekDataImport, postDataBulk } from "../../../actions/stakeholder.actions";
import moment from 'moment';
import {setStatus} from "../fnc_var";
import Swal from "sweetalert2";
import UpdData from "./updData";

function ImportData() {
  const [activeTab, setActiveTab] = useState('normal');
  const [selectedProgram, setSelectedProgram] = useState({})
  const program = useSelector(state => state.program)
  const _dataImport = useSelector(state => state.stakeholder?.dataImport)
  const user = useSelector(state => state.auth)
  const instansiId = user?.user?.user_instansi?user?.user?.user_instansi[0].instansi_id:0
  const [error, setError] = useState([]);
  const [dataImport, setDataImport] = useState([]);
  const [dataNormal, setDataNormal] = useState([]);
  const [dataWarning, setDataWarning] = useState([]);
  const [dataError, setDataError] = useState([]);
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch()
  
  const [showFormDetail, setShowFormDetail] = useState(false);
  const [showFormEdit, setShowFormEdit] = useState(false);
  const [showUpdateData, setShowUpdateData] = useState(false);

  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const list = dataImport?.map((i, index) => {
      return {
        col1: index + 1,
        col2: i.nik,
        col3: i.business_name,
        col4: i.name,
        col5: i.business_tlp,
        col6: i.business_category,
        col7: i.ket
      };
  });
  
  const data = React.useMemo(() => list, [dataImport]);
  const columns = React.useMemo(
    () => [
      {
        Header: 'No',
        accessor: 'col1',
        width: '5%',
        textAlign: 'center'
      },
      {
        Header: 'NIK/KTP',
        accessor: 'col2',
        width: '15%'
      },
      {
        Header: 'Nama Usaha',
        accessor: 'col3'
      },
      {
        Header: 'Nama Pemilik',
        accessor: 'col4',
        width: '15%',
        textAlign: 'center'
      },
      {
        Header: 'Kontak Usaha',
        accessor: 'col5',
        width: '15%',
        textAlign: 'center'
      },
      {
        Header: 'Ketegori Usaha',
        accessor: 'col6',
        width: '15%',
        textAlign: 'center',
        className:'capitalize'
      },
      {
        Header: 'Status Data',
        accessor: 'col7',
        width: '20%'
      }
    ],
    [dataImport]
  );

  useEffect(() => {
    // eslint-disable-next-line default-case
    switch (activeTab) {
      case 'normal':
        setDataImport(dataNormal)
      break;
      case 'warning':
        setDataImport(dataWarning)
      break;
      case 'error':
        setDataImport(dataError)
      break;
    }
  }, [activeTab])

  const handleFormDetail = (id,data,state) => { 
    const data_program = data.find(item => item.id === id);
    setSelectedProgram(data_program);
    switch (state) {
      case 'edit':
        setShowFormEdit(true);
      break;
      case 'view':
        setShowFormDetail(true);
      break;
    }
  }

  const handleDelete = (id) => { 
    Swal.fire({
      title: '',
      text: "Apakah anda Yakin ingin menghapus program tersebut?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Ya, Saya yakin',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batal',
  
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteProgram(id));
      }
    }) 
  }

  const dataRefresh = () => {     
    setDataImport([]);  
    setDataNormal([]);  
    setDataWarning([]);  
    setDataError([]);
  }

  const handleImportData = (data) => {
      setLoading(true);   
      var newDataNormal = []      
      var newDataWarning = []      
      var newDataError = []
      data.map((val, index) => {
        var obj = {};
            obj.nik = '' +val['Nomer Induk Kependudukan (NIK)'];
            obj.name = val['Nama Lengkap'];
            obj.address = val['Alamat Lengkap'];
            obj.no_tlp = '' + val['Nomer Telepon'];
            obj.email = val['Email'];
            obj.jenis_kelamin = val['Jenis Kelamin']==='Pria'?'l':(val['Jenis Kelamin']=='Wanita')?'p':'l';

            obj.business_name = val['Nama Bisnis'];
            obj.business_tlp = '' + val['Nomer Telepon Bisnis'];
            obj.business_address = val['Alamat Bisnis'];

            obj.business_type = val['Jenis Badan Usaha'];
            obj.business_category = val['Kategori Industri'];
            obj.status_data = 'normal';
            obj.ket = 'Data Normal';

        // validasi data kosong
        var ket1 = '';
        var ket3 = '';
        if([null,undefined ].includes(obj.nik)){ket1 += 'No Ktp, '}
        if([null,undefined ].includes(obj.business_name)){ket1 += 'Nama Bisnis, '}
        if([null,undefined ].includes(obj.business_tlp)){ket1 += 'Nomer Telepon Bisnis, '}
        if(obj.nik.length<16){ ket3='No KTP harus 16 digits' }

        if(ket3.length>0){
          obj.status_data = 'error';
          obj.ket = ket3;
          
          newDataError.push(obj)  
        } else if (ket1.length>0) {          
          ket1 = ket1.substring(0, ket1.length - 2) + ' tidak boleh kosong'          
          
          obj.status_data = 'error';
          obj.ket = ket1;
          
          newDataError.push(obj)  
        } else {
          newDataNormal.push(obj)
        }     
      });

      //cek duplikat data
      let seen = new Set();
      newDataNormal.some(function(val, index) {
        var ket2 = '';
        if (seen.size === seen.add(val.nik).size) {
          ket2 += 'No Ktp, '
        }
        if (seen.size === seen.add(val.business_name).size) {
          ket2 += 'Nama Bisnis, '
        }
        if (seen.size === seen.add(val.business_tlp).size) {
          ket2 += 'Nomer Telepon Bisnis, '
        }

        if (ket2.length>0) {
          ket2 = 'Duplikat '+ ket2.substring(0, ket2.length - 2) + ' di Excel'
          
          if (!(['error'].includes(newDataNormal[index].status_data))) {            
            newDataNormal[index].status_data = 'warning';
            newDataNormal[index].ket = ket2;    
            
            newDataWarning.push(newDataNormal[index])
            delete newDataNormal[index]
          }
        }
      });

      //cek Database
      var cekData = {}
      cekData.instansi_id = instansiId;
      cekData.created_by = 'umang';
      cekData.isCheck = true;
      cekData.data = newDataNormal;
      console.log('cekData ', cekData);
      dispatch(cekDataImport(cekData))
        .then((res) => {                
          setDataNormal(newDataNormal)
          setDataWarning(newDataWarning)
          setDataError(newDataError)
          setDataImport(newDataNormal)
          setLoading(false);
        })
        .catch((err) => {
          var eror = err.response.data.error
          console.log('err',eror);
          if(eror){
            Object.keys(eror).map((keyName, txt) => {
              var fields = keyName.split('.');
              console.log(newDataNormal[fields[1]]);
              eror[keyName].map((val, i) => {          
                newDataNormal[fields[1]].status_data = 'warning';
                newDataNormal[fields[1]].ket = val;    
                
                newDataWarning.push(newDataNormal[fields[1]])
                delete newDataNormal[fields[1]]
              })
            });
            
            setDataNormal(newDataNormal)
            setDataWarning(newDataWarning)
            setDataError(newDataError)
            setDataImport(newDataNormal)
            setLoading(false);
          }
        }
      )
  }
  
  const handleSaveData = () => {
    var newData = {};
    newData.instansi_id = instansiId;
    newData.created_by = 'umang';
    newData.data = dataNormal;
    
    if (dataNormal?.length>0) {
      setLoading(true);
      dispatch(postDataBulk(newData))
        .then((result) => {        
          setLoading(false);        
          Swal.fire({
            title: 'Success!',
            text: 'Selamat Data anda sudah berhasil diimport',
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Okay',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Buat Program Baru',
          }).then((res) => {
            if (result.isConfirmed) {
              window.location = "/stakeholder-rekap";
            }else{
              window.location = "/stakeholder-rekap";
              setDataImport([])
              setDataNormal([])
            }
          });
        })
        .catch((err) => {
          setLoading(false);
          Swal.fire({
            title: 'Error!',
            text: 'Gagal Mengimport data, cek apakah ada data yang error atau belum lengkap',
            icon: 'error',
            confirmButtonColor: '#16935B',
            confirmButtonText: 'Okay'
          })
        })
    } else {     
      setLoading(false); 
      Swal.fire({
        title: 'Error!',
        text: 'Tidak Terdapat Data, silahkan import file excel anda',
        icon: 'error',
        confirmButtonColor: '#16935B',
        confirmButtonText: 'Okay'
      })
    }
  }

  return (
    <>
      <Layout loading={loading}>
        <div className="row table-title">
          <span className="title">Stakeholder</span>
          <span className="desk">Stakeholder {'>'} Import Data</span>
        </div>
        <div className="row table-view">
          <div className="col">
              <div className="table-tittle">
                {dataImport.length>0?
                  <span className="title">Data Check Import</span>
                  :
                  <button className="btn btn-import" onClick={() => {setShowUpdateData(true) }}>
                    Import Data
                  </button>
                }
              </div>
              <div className="col-tab-wrapper col-tab-mrgn">
                <Tabs
                    id="controlled-tab-example"
                    activeKey={activeTab}
                    onSelect={(k) => setActiveTab(k)}
                    className="mb-1"
                    >
                    <Tab eventKey="normal" title={`Data Layak ${dataNormal.length>0?'('+dataNormal.length+')':''}`} className="text-green" />
                    <Tab eventKey="warning" title={`Data Bermasalah ${dataWarning.length>0?'('+dataWarning.length+')':''}`} className="text-yellow" />
                    <Tab eventKey="error" title={`Data Error ${dataError.length>0?'('+dataError.length+')':''}`} className="text-red" />
                </Tabs>
              </div>
              <DataTable
                data={data}
                columns={columns}
                loading={loading}
              />
              
              <div className="col-tab-action">
                {dataImport.length>0 &&
                  <>
                  {!(dataWarning.length>0 || dataError.length>0)?
                    <>
                    <button className="btn btn-sm button-green mr-2" onClick={() => {handleSaveData();}}>Simpan</button>
                    <button className="btn btn-sm button-primary " onClick={() => {dataRefresh()}}>Batal</button>
                    </>
                    :                    
                    <div className="col-span-error">
                      <span className="title">DATA TIDAK BISA DI IMPORT</span>
                      <span className="desk">Masih terdapat Data Bermasalah atau Error, Mohon Perbaiki data dan format pada File Excel anda, agar proses import data dapat berjalan.</span>
                    <button className="btn btn-sm button-primary " onClick={() => {dataRefresh()}}>Upload Ulang</button>
                    </div>
                  }  
                  </>
                }    
              </div>
          </div>
        </div>      
      </Layout>    
      <UpdData 
        isShow={showUpdateData} 
        handleSave={(val)=>{setShowUpdateData(false); setLoading(true); handleImportData(val); }}
        handleClose={() => setShowUpdateData(false)}
      />
    </>
  );
}

export default ImportData;
