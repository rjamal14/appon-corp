import React, { useEffect, useRef, useState } from "react";
import {FaAngleRight } from 'react-icons/fa'
import Modal from "../../../components/UI/Modal";
import "./style.scss"
import {setStatus, setVerif} from "../fnc_var";
import moment from 'moment';

const FormDetail = ({ isShow, handleClose, data }) => {
  return(
    <>
      <Modal modalTitle="Detail" size="lg" show={isShow} handleClose={() => handleClose()}>
        <div className="row">
          <div className="col">
                <div className="row modal-title">
                    <div className="col-12 col-md-12">   
                        <span className="modal-sub-title">
                            Profil Pengusaha
                        </span>
                    </div>
                </div>         
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                            <span className="title">Foto KTP</span>
                            { data?.photo_identity!==null &&
                            <img width={"100%"} height={"auto"} src={data?.photo_identity} />
                            }
                        </div>
                    </div>   
                </div>         
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">NIK/No KTP</span>
                        <span className="desk">{data?.nik??'-'}</span>
                        </div>
                    </div>   
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Nama Pemilik</span>
                        <span className="desk">{data?.name??'-'}</span>
                        </div>
                    </div>  
                </div> 
                <div className="row">
                    <div className="col-12 col-md-12">                        
                        <div className="form-group">
                        <span className="title">Alamat</span>
                        <span className="desk">{data?.address??'-'}</span>
                        </div>
                    </div> 
                </div>        
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Alamat Email</span>
                        <span className="desk">{data?.email??'-'}</span>
                        </div>
                    </div>   
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Nomor Kontak</span>
                        <span className="desk">{data?.no_telp??'-'}</span>
                        </div>
                    </div>  
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">NPWP Pribadi</span>
                        <span className="desk">{data?.npwp??'-'}</span>
                        </div>
                    </div>  
                </div> 

                <div className="row modal-title">
                    <div className="col-12 col-md-12">   
                        <span className="modal-sub-title">
                            Data Usaha
                        </span>
                    </div>
                </div>              
                <div className="row">
                    <div className="col-12 col-md-4">                         
                        <div className="form-group">
                            <span className="title">Foto Usaha</span>
                            { data?.photo_business!==null &&
                            <img width={"100%"} height={"auto"} src={data?.photo_business} />
                            }
                        </div>
                    </div>   
                </div>      
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Nama Usaha</span>
                        <span className="desk">{data?.business_name??'-'}</span>
                        </div>
                    </div>   
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Nomer Telepon Usaha</span>
                        <span className="desk">{data?.business_tlp??'-'}</span>
                        </div>
                    </div> 
                </div>      
                <div className="row">
                    <div className="col-12 col-md-12">                        
                        <div className="form-group">
                        <span className="title">Alamat</span>
                        <span className="desk">{data?.business_address??'-'}</span>
                        </div>
                    </div> 
                </div>   
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">NPWP Perusahaan</span>
                        <span className="desk">{data?.business_npwp??'-'}</span>
                        </div>
                    </div>   
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">NIB</span>
                        <span className="desk">{data?.business_nib??'-'}</span>
                        </div>
                    </div>  
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Kategori Usaha</span>
                        <span className="desk">{data?.business_category?.category??'-'}</span>
                        </div>
                    </div>  
                </div> 

                <div className="row modal-title">
                    <div className="col-12 col-md-12">   
                        <span className="modal-sub-title">
                            Data Verifikasi
                        </span>
                    </div>
                </div>            
                <div className="row">
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Status Verifikasi</span>
                        <span className="desk">{data?.status?setVerif(data.status):'-'}</span>
                        </div>
                    </div>  
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Verifikasi Melalui</span>
                        <span className="desk">{data?.verify_type??'-'}</span>
                        </div>
                    </div>   
                    <div className="col-12 col-md-4">                        
                        <div className="form-group">
                        <span className="title">Diverifikasi Oleh</span>
                        <span className="desk">{data?.verified_by??'-'}</span>
                        </div>
                    </div> 
                </div>      
                <div className="row">
                    <div className="col-12 col-md-12">                        
                        <div className="form-group">
                        <span className="title">Catatan Verifikasi</span>
                        <span className="desk">{data?.status_reason??'-'}</span>
                        </div>
                    </div> 
                </div> 

            <div className="float-right">
              <button className="btn btn-sm button-secondary mr-2" onClick={() => handleClose()}>Tutup</button>
            </div>
          </div>
        </div>
      </Modal>
    </>
    
  )
}

export default FormDetail