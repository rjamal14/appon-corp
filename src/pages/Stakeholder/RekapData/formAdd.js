import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editInstansi, getProfil } from "../../../actions/instansi.actions";
import Input from "../../../components/UI/Input";
import Modal from "../../../components/UI/Modal";
import "./style.scss"
import { typeVerifikasi, statusData, renderCategory } from "../fnc_var";
import { getCategory_Industry } from "../../../actions/category_industry.actions";
import { postData, updateData } from "../../../actions/stakeholder.actions";
import Compressor from 'compressorjs'
import Swal from "sweetalert2";

const FormAdd = ({ data, isShow, state, handleClose }) => {
  const [dataUmkm, setDataUmkm] = useState([]) 
  const error = useSelector(state => state.stakeholder?.error);
  const user = JSON.parse(localStorage.getItem("user"));
  const category_industry = useSelector(state => state.category_industry)
  const dispatch = useDispatch()

  useEffect(() => {  
      setDataUmkm([])
  }, [data])
  
  useEffect(() => {
    dispatch(getCategory_Industry())
    setDataUmkm([])
  }, [])

  function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }

  const handleSave = () => {
    const instanceId = user.user_instansi[0].instansi_id
    var newData = dataUmkm;
    newData.instansi_id = instanceId;
    newData.created_by = 'umang';
    newData.status = dataUmkm?.status??'new';
    newData.business_category = dataUmkm?.business_category?.id? dataUmkm?.business_category : (dataUmkm?.business_category??null);
    
    if (state==='edit') { //state editing      
      if(newData?.photo_identity?.indexOf('base64') == -1){
        delete newData.photo_identity
      }
      if(newData?.photo_business?.indexOf('base64') == -1){
        delete newData.photo_business
      }

      dispatch(updateData(newData,dataUmkm.id))
        .then((res) => {
          handleClose()
        })
        .catch((err) => {
          Swal.fire({
            title: 'Error!',
            text: 'Gagal mengupdate data, cek apakah ada data yang error atau belum lengkap',
            icon: 'error',
            confirmButtonColor: '#16935B',
            confirmButtonText: 'Okay'
          })
          console.log(err)
        })
    } else {
      dispatch(postData(newData))
        .then((res) => {
          handleClose()
        })
        .catch((err) => {
          Swal.fire({
            title: 'Error!',
            text: 'Gagal menyimpan data, cek apakah ada data yang error atau belum lengkap',
            icon: 'error',
            confirmButtonColor: '#16935B',
            confirmButtonText: 'Okay'
          })
        })
    }
  }

  const handleChange = e => {
    const {name, value} = e.target;
    setDataUmkm({...dataUmkm, [name]: value});
  }
  
  const handleChangeFile = e => {
    const {name, value} = e.target;
    if (e.target.files.length > 0) {
      const file = e.currentTarget.files[0]

      new Compressor(file, {
        quality: 0.6,
        success(result) {
          getBase64(result, (res) => {
            setDataUmkm({...dataUmkm, [name]: res});
          });
        },
        error(err) {
          console.log(err)
        }
      })
    }
  }

  return(
    <>
      <Modal modalTitle="Tambah Data" size="lg" show={isShow} handleClose={() => handleClose()}>
        <div className="row">
          <div className="col">    
            <div className="row modal-title">
              <div className="col-12 col-md-12">   
                  <span class="modal-sub-title">
                    Profil Pengusaha
                  </span>
              </div>
            </div>      
            <div className="row">
              <div className="col-12 col-md-4"> 
                { dataUmkm?.photo_identity!==null &&
                  <img width={"100%"} height={"auto"} src={dataUmkm?.photo_identity} />
                }
                <Input
                      type="file"
                      label="Foto KTP"
                      name="photo_identity"
                      onChange={handleChangeFile}
                    />
              </div>   
            </div> 
            <div className="row">
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="NIK/KTP"
                    name="nik"
                    error={error?.nik??''}
                    value={dataUmkm?.nik??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Nama Pemilik"
                    name="name"
                    error={error?.name??''}
                    value={dataUmkm?.name??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="row">
              <div className="col-12 col-md-12">   
                  <Input
                    type="input"
                    label="Alamat"
                    name="address"
                    value={dataUmkm?.address??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="row">
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Alamat Email"
                    name="email"
                    value={dataUmkm?.email??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Nomor Kontak"
                    name="no_tlp"
                    error={error?.no_telp??''}
                    value={dataUmkm?.no_tlp??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="NPWP Pribadi"
                    name="npwp"
                    value={dataUmkm?.npwp??''}
                    onChange={handleChange}
                  />
              </div>
            </div>  

            <div className="row modal-title">
              <div className="col-12 col-md-12">   
                  <span class="modal-sub-title">
                    Data Usaha
                  </span>
              </div>
            </div>       
            <div className="row">
              <div className="col-12 col-md-4"> 
                { dataUmkm?.photo_business!==null &&
                  <img width={"100%"} height={"auto"} src={dataUmkm?.photo_business} />
                }
                <Input
                      type="file"
                      label="Foto Usaha"
                      name="photo_business"
                      onChange={handleChangeFile}
                    />
              </div>   
            </div>     
            <div className="row">
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Nama Usaha"
                    name="business_name"
                    error={error?.business_name??''}
                    value={dataUmkm?.business_name??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Nomer Telepon Usaha"
                    name="business_tlp"
                    error={error?.business_tlp??''}
                    value={dataUmkm?.business_tlp??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="row">
              <div className="col-12 col-md-12">   
                  <Input
                    type="input"
                    label="Alamat Usaha"
                    name="business_address"
                    value={dataUmkm?.business_address??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="row">
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="NPWP Perusahaan"
                    name="business_npwp"
                    value={dataUmkm?.business_npwp??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="NIB"
                    name="business_nib"
                    value={dataUmkm?.business_nib??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4"> 
                  <Input
                    type="select"
                    label="Kategori Usaha"
                    name="business_category"
                    options={renderCategory(category_industry,dataUmkm?.business_category??null)}
                    value={dataUmkm?.business_category??null}
                    onChange={handleChange}
                  />
              </div>
            </div>
            
            <div className="row modal-title">
              <div className="col-12 col-md-12">   
                  <span class="modal-sub-title">
                    Verifikasi Data
                  </span>
              </div>
            </div>     
            <div className="row">
              <div className="col-12 col-md-4">
                  <Input
                    type="select"
                    label="Status Verifikasi"
                    name="status"
                    options={statusData(dataUmkm?.status)}
                    value={dataUmkm?.status??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">
                  <Input
                    type="select"
                    label="Verifikasi Melalui"
                    name="verify_type"
                    options={typeVerifikasi(dataUmkm?.verify_type)}
                    value={dataUmkm?.verify_type??''}
                    onChange={handleChange}
                  />
              </div>
              <div className="col-12 col-md-4">   
                  <Input
                    type="input"
                    label="Diverifikasi Oleh"
                    name="verified_by"
                    value={dataUmkm?.verified_by??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="row">
              <div className="col-12 col-md-12">   
                  <Input
                    type="input"
                    label="Catatan Verifikasi"
                    name="status_reason"
                    value={dataUmkm?.status_reason??''}
                    onChange={handleChange}
                  />
              </div>
            </div> 
            <div className="float-right">
              <button className="btn btn-sm button-primary mr-2" onClick={() => handleClose()}>Batal</button>
              <button className="btn btn-sm button-green" onClick={() => handleSave()}>Simpan</button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default FormAdd