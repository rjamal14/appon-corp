export const setNumberFormat = (val) => {
  return val>0?val.toLocaleString():0;
};

export const setStatus = (val) => {
  switch (val) {
    case 'duplicate_excel':
      return 'Duplikat, data di file excel'
    break;    
    case 'duplicate_db':
      return 'Duplikat, data ini sudah ada di aplikasi'
    break;    
    case 'error':
      return 'Error, Ubah data di file excel anda'
    break;  
    default:
      return 'Data Normal'
    break;
  }
}

export const setVerif = (val) => {
  switch (val) {
    case 'linked':
      return 'Wirausaha (AppOn)'
    break;    
    case 'verified':
      return 'Terverifikasi'
    break;    
    case 'cancel':
      return 'Tidak Terverifikasi'
    break;  
    default:
      return 'Baru'
    break;
  }
}

export const typeVerifikasi = (val) => {
    const type = [{
      value:'',
      name:'Pilih tipe verifikasi',
      disabled:true,
      selected:true
    },{
      value:'Telepon',
      name:'Telepon',
      selected:val==='Telepon'?true:false
    },
    {
      value:'Whatsapp',
      name:'Whatsapp',
      selected:val==='Whatsapp'?true:false
    },
    {
      value:'Verifikasi Lapangan',
      name:'Verifikasi Lapangan',
      selected:val==='Verifikasi Lapangan'?true:false
    }]
    return type
}

export const statusData = (val) => {
    const type = [{
      value:'new',
      name:'Baru',
      selected:val==='new'?true:false
    },{
      value:'verified',
      name:'Terverifikasi',
      selected:val==='verified'?true:false
    },
    {
      value:'cancel',
      name:'Tidak Terverifikasi',
      selected:val==='cancel'?true:false
    },
    {
      value:'linked',
      name:'Wirausaha (Appon)',
      selected:val==='linked'?true:false
    }]
    return type
}
export const renderCategory = (data, selected) => {
  const type = [{
    value:'',
    name:'Pilih Kategori Usaha',
    disabled:true,
    selected:true
  }]
  if (data?.category_industry?.length>0) {
    data.category_industry.map((val,index) => {
      type.push({
        value: val.id,
        name: val.category,
        selected: val.id===selected?true:false
      })
    })
  }

  return type
}
