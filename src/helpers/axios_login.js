import axios from 'axios';
import { api_login } from '../urlConfig';
import store from '../store';
import { authConstants } from '../actions/constants';
import env from '../config/env';

const token = window.localStorage.getItem('token');

const axiosIntance = axios.create({
    baseURL: env.authUrl,
    headers: {
        'Authorization': token ? `Bearer ${token}` : ''
    }
});

axiosIntance.interceptors.request.use((req) => {
    const { auth } = store.getState();
    if(auth.token){
        req.headers.Authorization = `Bearer ${auth.token}`;
    }
    return req;
})

axiosIntance.interceptors.response.use((res) => {
    return res;
}, (error) => {
    // console.log(error.response);
    const status = error.response ? error.response.status : 500;
    if(status && (status === 500 || status === 400)){
        // localStorage.clear();
        store.dispatch({ type: authConstants.LOGIN_FAILURE });
    }
    return Promise.reject(error);
})

export default axiosIntance;