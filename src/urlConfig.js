const baseUrl = process.env.API || "https://scorp.appon.co.id";

export const api = `${baseUrl}/api/v1`;

export const generatePublicUrl = (fileName) => {
  return `${baseUrl}/public/${fileName}`;
};

export const api_login = `https://sauth.appon.co.id`;
